from __future__ import absolute_import, unicode_literals
import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ariana_imdb.settings')
BROKER_URL = 'redis://localhost:6379/0'

app = Celery('ariana_imdb', broker=BROKER_URL)

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()
