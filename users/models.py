from django.db import models
from django.conf import settings

from django.utils.translation import ugettext as _


class ImdbUser(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    first_name = models.CharField(
        _("first name"), max_length=150, null=True, blank=True
    )
    last_name = models.CharField(_("last name"), max_length=150, null=True, blank=True)
    email = models.EmailField(_("email address"), null=False, blank=False)
    phone_number = models.CharField(
        _("phone number"),
        max_length=15,
        null=False,
        blank=False,
    )

    class Meta:

        verbose_name = _("imdb user")
        verbose_name_plural = _("imdb users")
        db_table = "imdb_user"

    @property
    def full_name(self):
        return f'{self.first_name} {self.last_name}'

    def __str__(self):
        return self.full_name
