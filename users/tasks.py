from ariana_imdb.celery import app
from django.core.mail import send_mail


@app.task(name="email_user")
def email_user(subject, message, recipient_list, from_email=None):
    """
    Sends an email.
    """
    send_mail(subject, message, from_email, recipient_list, fail_silently=False)




