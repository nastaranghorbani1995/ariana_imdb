from django.contrib import admin

from .models import ImdbUser


@admin.register(ImdbUser)
class ImdbUserAdmin(admin.ModelAdmin):
    pass

