from django.db import models

from django.utils.translation import ugettext as _

from django.conf import settings

from users.models import ImdbUser

from users.tasks import email_user

from django.dispatch import receiver

from django.db.models.signals import post_save, post_delete, pre_save

class Movie(models.Model):
    title = models.CharField(_("title"), max_length=100, null=False, blank=False)
    description = models.TextField(_("description"), null=False, blank=False)
    release_date = models.DateField(_("release date"), null=True, blank=True)
    vote_average = models.FloatField(_("vote average"), null=False, blank=False)
    director = models.CharField(_("director"), max_length=100, null=False, blank=False)

    class Meta:

        verbose_name = _("movie")
        verbose_name_plural = _("movies")
        db_table = "movie"

    def __str__(self):
        return self.title


@receiver(post_save, sender=Movie)
def send_email_save(sender, instance, **kwargs):
    all_emails = list(ImdbUser.objects.values('email'))
    subject = "new movie is added!"
    message = f"{instance.title} is added"

    email_user.delay(subject=subject, message=message,
                     from_email=settings.EMAIL_HOST_USER,
                     recipient_list=all_emails)


@receiver(post_delete, sender=Movie)
def send_email_otp(sender, instance, **kwargs):
    all_emails = list(ImdbUser.objects.values('email'))
    subject = "a movie has been deleted"
    message = f"{instance.title} deleted"
    email_user.delay(subject=subject, message=message,
                     from_email=settings.EMAIL_HOST_USER,
                     recipient_list=all_emails)


@receiver(pre_save, sender=Movie)
def send_email_update(sender, instance ,**kwargs):
    all_emails = list(ImdbUser.objects.values('email'))
    subject = "a movie has been updated"
    message = f"{instance.title} updated"
    email_user.delay(subject=subject, message=message,
                     from_email=settings.EMAIL_HOST_USER,
                     recipient_list=all_emails)