from django.urls import path

from movies.views import create_movie, movie_list, delete_movie, edit_movie

urlpatterns = [
    path("create-movie", create_movie),
    path("movie-list", movie_list),
    path("delete-movie/<int:movie_id>", delete_movie),
    path("edit-movie/<int:movie_id>", edit_movie)

]
