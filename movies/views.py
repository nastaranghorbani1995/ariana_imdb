from django.utils.translation import ugettext as _

from rest_framework.decorators import api_view, throttle_classes, permission_classes
from rest_framework.throttling import UserRateThrottle
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import status
from django.contrib.auth import get_user_model

from django.db.models import Q

from django.conf import settings

from .models import Movie
from .serializers import MovieSerializer
from users.tasks import email_user
from users.models import ImdbUser


@throttle_classes([UserRateThrottle])
@api_view(["GET"])
@permission_classes([AllowAny])
def movie_list(request):
    try:
        search = request.GET.get("search", "")

        movies = Movie.objects.all()

        if search:
            movies = Movie.objects.filter(
                Q(title__contains=search) | Q(director__contains=search)
            )

        serializer = MovieSerializer(instance=movies, many=True)

        return Response(
            {
                "status": True,
                "data": serializer.data,
                "message": _("success"),
            },
            status=status.HTTP_200_OK,
        )

    except Exception as e:
        return Response(
            {"status": False, "data": None, "message": str(e)},
            status=status.HTTP_400_BAD_REQUEST,
        )


@throttle_classes([UserRateThrottle])
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def create_movie(request):
    user_id = request.user.id
    try:
        user = get_user_model().objects.get(id=user_id)
        if user.is_staff:
            all_emails = list(ImdbUser.objects.values('email'))
            serializer = MovieSerializer(data=request.data)

            if not serializer.is_valid():
                return Response(
                    {"status": False, "data": None, "message": serializer.errors},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            serializer.save()

            return Response(
                {"status": True, "data": serializer.data, "message": _("success")},
                status=status.HTTP_201_CREATED,
            )
        else:
            return Response(
                {"status": False, "data": None, "message": _("authorization failed")},
                status=status.HTTP_403_FORBIDDEN,
            )
    except Exception as e:
        return Response(
            {"status": False, "data": None, "message": str(e)},
            status=status.HTTP_400_BAD_REQUEST,
        )


@throttle_classes([UserRateThrottle])
@api_view(["DELETE"])
@permission_classes([IsAuthenticated])
def delete_movie(request, movie_id):
    try:
        user_id = request.user.id
        movie = Movie.objects.get(id=movie_id)

        user = get_user_model().objects.get(id=user_id)
        if user.is_superuser:
            movie.delete()
            return Response(
                {"status": True, "data": None, "message": "success"},
                status=status.HTTP_204_NO_CONTENT,)

        else:
            return Response(
                {"status": False, "data": None, "message": "forbidden"},
                status=status.HTTP_403_FORBIDDEN,
            )
    except Exception as e:
        return Response(
            {"status": False, "data": None, "message": str(e)},
            status=status.HTTP_400_BAD_REQUEST,
        )


@throttle_classes([UserRateThrottle])
@api_view(["PUT"])
@permission_classes([IsAuthenticated])
def edit_movie(request, movie_id):
    try:
        movie = Movie.objects.get(id=movie_id)
        serializer = MovieSerializer(instance=movie, data=request.data)
        if not serializer.is_valid():
            return Response(
                {"status": False, "data": None, "message": serializer.errors},
                status=status.HTTP_400_BAD_REQUEST,
            )
        serializer.save()

        return Response(
            {"status": True, "data": serializer.data, "message": _("success")},
            status=status.HTTP_200_OK,
        )
    except Exception as e:
        return Response(
            {"status": False, "data": None, "message": str(e)},
            status=status.HTTP_400_BAD_REQUEST,
        )
